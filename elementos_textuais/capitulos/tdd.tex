%% ADAPTADO DE: 
%% abtex2-modelo-include-comandos.tex, v-1.6 laurocesar
%% Copyright 2012-2013 by abnTeX2 group at http://abntex2.googlecode.com/ 
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%% 
%% The Current Maintainer of this work is the abnTeX2 team, led
%% by Lauro César Araujo. Further information are available on 
%% http://abntex2.googlecode.com/
%%
%% This work consists of the files abntex2-modelo-include-comandos.tex

% ---
% Este capítulo, utilizado por diferentes exemplos do abnTeX2, ilustra o uso de
% comandos do abnTeX2 e de LaTeX.
% ---
 
\chapter{TDD - Desenvolvimento Guiado por Testes}\label{cap_tdd}

O TDD - Desenvolvimento Guiado por Testes - é uma estratégia que orienta o desenvolvedor a executar testes em todo processo de desenvolvimento de um software. É baseado em uma ideia simples, mas que muda radicalmente a forma tradicional de desenvolvimento, faz com que os testes de software sejam desenvolvidos antes mesmo da codificação das aplicações e introduz as atividades de testagem a cada trecho de código escrito.
\begin{citacao}O desenvolvimento Guiado por Teste (TDD) é uma ideia ilusoriamente simples: escreva os testes para o seu código antes de escrever o código em si. Dizemos ilusoriamente simples porque isso transforma o papel da testante no processo de desenvolvimento e desafia as suposições da nossa indústria quanto ao objetivo da testagem. \cite[5.3]{prycefreeman}.
\end{citacao}
A prática do TDD é amplamente utilizado nas abordagens conhecidas como metodologias de desenvolvimento ágil de software. ``Ele é uma prática central de Extreme Programming (XP - Programação Extrema), é recomendaod por Crystal Clear e, com frequência, é usado em projetos Scrum''.\cite{prycefreeman}.

% ---
\section{Surgimento do TDD}
% ---

A proposta do TDD foi formalizada em 2002 pelo autor Kent Beck em seu livro TDD - Test-Driven Development by Example, mas teve início na década de 90, quando Kent Beck desenvolve, na linguagem SmallTalk, uma biblioteca de testes chamada sUnit. Com o propósito de facilitar a execução de testes, a biblioteca tornava esta atividade que antes era feita manualmente no processo de desenvolvimento, uma tarefa automatizada.

\begin{citacao}
SUnit allows one to write the tests and check results in Smalltalk; while this approach has the disadvantage that testers need to be able to write simple Smalltalk programs, the resulting tests are very stable [3]
\end{citacao}

A popularização da automação de testes surge alguns anos depois, quando em conjunto, Erich Gamma, autor conhecido pela publicação do livro Design Patterns: Elements of Reusable Object-Oriented Software, e Beck, criam uma versão da biblioteca sUnit para a linguagem de programação Java e a chamam de jUnit. Essa ferramenta fomentou o interesse pela automação de testes e serviu de base para o desenvolvimento de diversas outras ferramentas de diferentes linguagens de programação, como Ruby, C++, Python e PHP. E seu uso deixou de ser visto somente como uma atividade de teste e passou a ser visto como uma atividade de projeto de código-fonte.

\begin{citacao}
Em sistemas de qualquer tamanho interessante, a testagem manual costumeira é simplesmente pouco prática; portanto, precisamos automatizá-la tanto quanto possível para reduzir os cursos de montagem, exposição e modificações de versões do sistema. \cite[5.3]{prycefreeman}.
\end{citacao}

Assim a proposta de escrever os testes que especificam o funcionamento de determinado método ou classe antes da implementação do código-fonte ganhou mais atenção, o uso das ferramentas automatizadas de teste forneceu a possibilidade de desenvolver o código e realizar testes até que o mesmo seja aceito, indicando para o programador durante o processo de desenvolvimento se o método ou classe implementado está de acordo com o que deseja. 

\begin{citacao}
Ao passo que o uso das bibliotecas xUnit foi amadurecendo, utilizá-las não era mais visto como apenas uma atividade de teste, mas sim como uma atividade de projeto de código. Essa visão faz sentido ao se pensar que antes de implementar um determinado método ou classe, os usuários de xUnit primeiro escrevem um teste especificando o comportamento esperado, e só então desenvolvem o código que vai fazer com que esse teste passe. \cite[5.3]{cucumberruby}
\end{citacao}

Kent Beck ainda na década 90 formaliza um conjunto de práticas, valores e princípios de desenvolvimento de software conhecida como Extreme Programming (XP), que entre suas práticas utiliza técnicas que viriam a fazer parte da do TDD, aumentando o interesse na publicação da estratégia e sua aprovação por comunidades de desenvolvimento de software, principalmente em projetos conhecidos como Open Source (Código Aberto).

% ---
\section{Motivações para o uso do TDD}
% ---

\subsection{O teste como atividade de projeto de código-fonte}

Escrevendo testes antes de escrever o código-fonte do software, estamos transformando o teste em uma atividade fundamental para o projeto de código, deixamos de testar somente o resultado de todo um processo de desenvolvimento para testar cada pequena parte desenvolvida, o que dará mais garantia de qualidade final no software.
A prática ainda fornece uma ferramenta que facilita o entendimento da funcionalidade que será desenvolvida, escrever o teste antes faz com que o desenvolvedor compreenda melhor o que o código-fonte deverá fazer e construa um conjunto de ideias para resolver o problema antes que comece a implementação, o que pode gerar grande agilidade no desenvolvimento. "Usamos os testes para clarear nossas ideias em relação a o que queremos que o código-fonte faça". \cite[5.3]{prycefreeman}.


\subsection{Confiança em alterações do código-fonte}

No decorrer do desenvolvimento, de forma automática, o desenvolvedor estará construindo um conjunto de testes que dará garantia do funcionamento de todas partes de um software. Esta técnica fornece maior confiança na modificação do software, o que encoraja mudanças no projeto de código-fonte e melhorias em até partes já desenvolvidas e em funcionamento.

\begin{citacao}
Se escrevermos testes durante todo o processo de desenvolvimento, podemos montar uma rede de segurança de testes de regressão automatizados que nos dá confiança para fazer alterações. \cite[5.3]{prycefreeman}.
\end{citacao}

\begin{citacao}
Uma das consequências de se desenvolver utilizando TDD é que o seu sistema fica coberto por uma suíte de testes automatizados, de modo que toda vez que você fizer uma mudança no código, é possível rodar a suíte e ela dirá se você quebrou algum comportamento previamente implementado. \cite[5.3]{cucumberruby}
\end{citacao}

Com os testes a introdução de novos membros em uma equipe de desenvolvimento se torna mais produtiva, mesmo não sabendo o funcionamento total do software os novos desenvolvedores terão maior contribuição no processo pois o conjunto de testes dará confiança as suas alterações. Segundo \cite{cucumberruby}, com a suíte de testes, novos desenvolvedores não precisam ter medo de mudar o código.

\subsection{Refatoração}

O TDD proporciona práticas que vão além do teste contínuo. A refatoração é uma técnica que busca otimizar o código-fonte após escrito, prática que altera a estrutura de um código-fonte sem modificar seu comportamento. É empregada para melhora constante da qualidade do código-fonte do projeto.
A atividade de refatorar faz parte do TDD na etapa em que a classe ou método escrito passou em seu teste, assim que o desenvolvedor tem garantia do comportamento do código-fonte ele poderá altera-lo para simplificar e aperfeiçoar seu projeto, remover os chamados DRY's (Don't Repeat Yourself), trechos ambíguos no código-fonte, e garantir que sua implementação fique clara para possíveis próximos desenvolvedores.

Ao aplicar a refatoração, temos que ter certeza de que o comportamento do software não será alterado. Segundo \cite{fowler1999refatoraccao} "O comportamento é a coisa mais importante acerca de software. É algo do qual os usuários dependem". Essa certeza é dada a partir dos testes escritos.
\begin{citacao}
Quando aplicamos a refatoração, temos de nos apoiar nos testes. [..] É essencial para a refatoração que você tenha bons testes. Vale a pena gastar o tempo que for necessário para a criação dos testes, porque eles lhe dão a segurança que você precisa para alterar o programa mais tarde. \cite{fowler1999refatoraccao}.
\end{citacao}

Um sistema mal projetado normalmente possui mais código-fonte do que necessário, a refatoração diminui o código-fonte, isso porque na maioria das vezes códigos são duplicados no decorrer do desenvolvimento. "Eliminando as cópias, você se assegura de que o código fiz tudo uma e apenas uma vez, o que é a essência do bom projeto". \cite{fowler1999refatoraccao}.

Com a refatoração o custo de manutenção do código-fonte pode diminuir, segundo \cite{boehm1981software} o custo de alteração em um projeto de software cresce de forma exponencial durante o avanço de suas fases, mas com um projeto de software bom, refletindo um código-fonte bem refatorado, \cite{cucumberruby} diz que o custo pode não seguir o crescimento exponencial.

\subsection{Custos e Tempo}

As metodologias ágeis de desenvolvimento buscam, por mais de todos os fatores propostos, a diminuição de custos e tempo.
No livro Software Engineering Economics, Barry Boehm sugere que o custo de alteração do software cresce de forma exponencial durante o seu ciclo de desenvolvimento, Roger S. Pressman, afirma que "Os custos de mudanças aumentam de forma não linear conforme o projeto avança" \cite{pressman}. O TDD, em conjunto com outras práticas de desenvolvimento ágil bem elaboradas, é uma garantia de que os custos e tempo para a manutenção não tenham um crescimento elevado.


\begin{figure}[htb]
	\caption{\label{fig_grafico}Custos de alterações como uma função do tempo em desenvolvimento}
	\begin{center}
	    \includegraphics[scale=1]{imagens/capitulos/tdd/custo_desenvolvimento.png}
	\end{center}
	\legend{Fonte: \citeonline[p. 83]{pressman}}
\end{figure}


% ---
\section{Aplicando o TDD}
% ---

O Ciclo básico para aplicar o TDD no desenvolvimento de projetos, pela visão do desenvolvedor, deve seguir três etapas:

\begin{enumerate}[label=(\alph*)]
    \item Escrever um teste de unidade
    \item Escrever o código-fonte que passe pelo teste
    \item Refatorar buscando a simplificação do código escrito.
\end{enumerate}



\begin{figure}[htb]
	\caption{\label{fig_grafico}Ciclo básico do TDD}
	\begin{center}
	    \includegraphics[scale=0.3]{imagens/capitulos/tdd/ciclo_tdd.jpg}
	\end{center}
	\legend{Fonte: \citeonline[p. 6]{prycefreeman}}
\end{figure}

